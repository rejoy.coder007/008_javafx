
function fn() {
    // "On document ready" commands:
    console.log(document.readyState);



/////////////////////////  Function abstraction //////////////////////////////

   //λa.b
    // b is body
   /// ..The name after λ is the argument identifier and the expression after point (.)
    // represents the body of the function. I
    console.log("##################### λx.x ##################")

     var sample = function(a) {
        a = a+100;
        return a;
    };


    var sampleF = a => {
        a = a+100;
        return a;
    }

    console.log(sampleF(20));


    console.log("##################### λy.y² ##################")

    var sample1 = function(y) {
        return y*y;
    };


    var sampleF1 = y => y*y;

    console.log(sampleF1(2));

/////////////////////////  Function Application //////////////////////////////

    const E = y => (y ** 2);

    const A = 5;

// The expression "A" replaces the variable "y" in the application.
// Thus, the function can be simplified as "A²".

 //   E(A) == (A ** 2);    // true

 //   A ** 2   ;           // 25

    console.log(E(A))     ;            // 25
}




// `DOMContentLoaded` may fire before your script has a chance to run, so check before adding a listener
if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", fn);
} else {  // `DOMContentLoaded` already fired
    fn();
}



