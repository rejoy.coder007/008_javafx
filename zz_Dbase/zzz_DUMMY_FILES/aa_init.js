console.log(sampleF(20));






/*
     x                      // A variable name
     λx.x                   // A Lambda function
     λx.x 5                 // Function application. Number
                            // 5 applied to the function.
 */
// An anonymous function
var fun2= (x => x)(5);
var x =7;                       // A variable name
var fun1= x => x ;          // Function application. Number
                            // 5 applied to the function.

console.log(x);
console.log(fun1(100));
console.log(fun2);


/////////////////////////  Function abstraction //////////////////////////////

//λa.b
// b is body
/// ..The name after λ is the argument identifier and the expression after point (.)
// represents the body of the function. I
console.log("##################### λx.x ##################")

var sample = function(a) {
    a = a+100;
    return a;
};


var sampleF = a => {
    a = a+100;
    return a;
}

console.log(sampleF(20));


console.log("##################### λy.y² ##################")

var sample1 = function(y) {
    return y*y;
};


var sampleF1 = y => y*y;

console.log(sampleF1(2));

/////////////////////////  Function Application //////////////////////////////

const E = y => (y ** 2);

const A = 5;

// The expression "A" replaces the variable "y" in the application.
// Thus, the function can be simplified as "A²".

//   E(A) == (A ** 2);    // true

//   A ** 2   ;           // 25

console.log(E(A))     ;            // 25




import forEach from "./LIB/functional_module.js";
