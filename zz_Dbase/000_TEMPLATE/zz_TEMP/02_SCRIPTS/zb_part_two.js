
const forEach = (array,fn) => {
    let i;
    for(i=0;i<array.length;i++)
        fn(array[i]);
};


function fn() {
    // "On document ready" commands:
    console.log(document.readyState);

    var simpleFn = () => "Simple Function";
    console.log(simpleFn());


    var simpleFn1 = () => {
        let value = "Simple Function";
        return value;
    };//for multiple statement wrap with { }

    console.log(simpleFn1());


    var simpleFn2 = () => { //function scope
        if (true) {
            let a = 1;
            var b = 2;
            console.log(a);
            console.log(b);
        } //if block scope
        console.log("var" + b);//function scope;
        // console.log("let"+a); //function scope
    };

    simpleFn2();


    var array = [1,2,3];
    forEach(array,(data) => console.log(data)); //refereing to imported forEach



}




// `DOMContentLoaded` may fire before your script has a chance to run, so check before adding a listener
if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", fn);
} else {  // `DOMContentLoaded` already fired
    fn();
}




//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX2luaXQuanMiLCJ6el9iYXNpYy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJ6Yl9wYXJ0X3R3by5qcyIsInNvdXJjZXNDb250ZW50IjpbIiIsImNvbnN0IGZvckVhY2ggPSAoYXJyYXksZm4pID0+IHtcbiAgICBsZXQgaTtcbiAgICBmb3IoaT0wO2k8YXJyYXkubGVuZ3RoO2krKylcbiAgICAgICAgZm4oYXJyYXlbaV0pO1xufTtcblxuXG5mdW5jdGlvbiBmbigpIHtcbiAgICAvLyBcIk9uIGRvY3VtZW50IHJlYWR5XCIgY29tbWFuZHM6XG4gICAgY29uc29sZS5sb2coZG9jdW1lbnQucmVhZHlTdGF0ZSk7XG5cbiAgICB2YXIgc2ltcGxlRm4gPSAoKSA9PiBcIlNpbXBsZSBGdW5jdGlvblwiO1xuICAgIGNvbnNvbGUubG9nKHNpbXBsZUZuKCkpO1xuXG5cbiAgICB2YXIgc2ltcGxlRm4xID0gKCkgPT4ge1xuICAgICAgICBsZXQgdmFsdWUgPSBcIlNpbXBsZSBGdW5jdGlvblwiO1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgfTsvL2ZvciBtdWx0aXBsZSBzdGF0ZW1lbnQgd3JhcCB3aXRoIHsgfVxuXG4gICAgY29uc29sZS5sb2coc2ltcGxlRm4xKCkpO1xuXG5cbiAgICB2YXIgc2ltcGxlRm4yID0gKCkgPT4geyAvL2Z1bmN0aW9uIHNjb3BlXG4gICAgICAgIGlmICh0cnVlKSB7XG4gICAgICAgICAgICBsZXQgYSA9IDE7XG4gICAgICAgICAgICB2YXIgYiA9IDI7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhhKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGIpO1xuICAgICAgICB9IC8vaWYgYmxvY2sgc2NvcGVcbiAgICAgICAgY29uc29sZS5sb2coXCJ2YXJcIiArIGIpOy8vZnVuY3Rpb24gc2NvcGU7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwibGV0XCIrYSk7IC8vZnVuY3Rpb24gc2NvcGVcbiAgICB9O1xuXG4gICAgc2ltcGxlRm4yKCk7XG5cblxuICAgIHZhciBhcnJheSA9IFsxLDIsM107XG4gICAgZm9yRWFjaChhcnJheSwoZGF0YSkgPT4gY29uc29sZS5sb2coZGF0YSkpOyAvL3JlZmVyZWluZyB0byBpbXBvcnRlZCBmb3JFYWNoXG5cblxuXG59XG5cblxuXG5cbi8vIGBET01Db250ZW50TG9hZGVkYCBtYXkgZmlyZSBiZWZvcmUgeW91ciBzY3JpcHQgaGFzIGEgY2hhbmNlIHRvIHJ1biwgc28gY2hlY2sgYmVmb3JlIGFkZGluZyBhIGxpc3RlbmVyXG5pZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSA9PT0gXCJsb2FkaW5nXCIpIHtcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCBmbik7XG59IGVsc2UgeyAgLy8gYERPTUNvbnRlbnRMb2FkZWRgIGFscmVhZHkgZmlyZWRcbiAgICBmbigpO1xufVxuXG5cblxuIl19
