const forEach = (array,fn) => {
   let i;
   for(i=0;i<array.length;i++)
      fn(array[i]);
};

export default forEach;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJlczYtZnVuY3Rpb25hbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBmb3JFYWNoID0gKGFycmF5LGZuKSA9PiB7XG4gICBsZXQgaTtcbiAgIGZvcihpPTA7aTxhcnJheS5sZW5ndGg7aSsrKVxuICAgICAgZm4oYXJyYXlbaV0pO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgZm9yRWFjaDtcbiJdLCJmaWxlIjoiZXM2LWZ1bmN0aW9uYWwuanMifQ==
