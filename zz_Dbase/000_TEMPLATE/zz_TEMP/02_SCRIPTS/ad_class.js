


class User {



    constructor(email, name){
        this.email = email;
        this.name = name;
    }


}

User.prototype.login = function(){

    console.log(this.email, 'has logged in');
};


var userOne = new User('ryu@ninjas.com', 'Ryu');
var userTwo = new User('yoshi@mariokorp.com', 'Yoshi');

userOne.login();


