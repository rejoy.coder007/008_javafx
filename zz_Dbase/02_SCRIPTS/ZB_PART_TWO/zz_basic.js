
function fn() {
    // "On document ready" commands:
    console.log(document.readyState);

    function sample(a){
        a = a+100;
        return a;
    }

// Syntax for JavaScript Functions
    var sample1 = function(a) {
        a = a+100;
        return a;
    };


    var sample2 = a => {
        a = a+100;
        return a;
    };

    var sample2 = a => {
        a = a+100;
        return a;
    };

    var sample3 =  a => (b =>{
        a = a+100+b;
        return a;
    }) ;

    var sample4 = (a => {
        a = a+100;
        return a;
    })(4000);

    console.log(sample(1000));
    console.log(sample1(2000));
    console.log(sample2(3000));
    console.log(sample3(4000)(5000));
    console.log(sample4);
   // console.log(sample4(5000));// This will show error


}




// `DOMContentLoaded` may fire before your script has a chance to run, so check before adding a listener
if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", fn);
} else {  // `DOMContentLoaded` already fired
    fn();
}



