<?php

$factory->define(App\Models\Aa_user::class, function (Faker\Generator $faker) {
    return [

        'username' => $faker->userName,
        'password' => $faker->password,
        'fullName' =>$faker->firstName,


        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'startDate' =>$faker->date(),
        'salary' => $faker->numberBetween(1000,2000),
        'userType' =>"user"
    ];
});

$factory->define(App\Models\Ab_reservation::class, function (Faker\Generator $faker) {


    $start_date =$faker->dateTimeBetween('+2 days', '+35 days');
    $end_date =$faker->dateTimeBetween($start_date, '+35 days');




    //$datetime1 = new DateTime((string)$start_date);
    //$datetime2 = new DateTime((string)$end_date);
    $interval = $start_date->diff($end_date);



    $numberDays = $interval->days;



    $roomType = ["normal","semi delux","premium"];
    $price= [1000,2000,4000];
    $room_random = $faker->numberBetween(0,count($roomType)-1);
    $cost =   $price[$room_random]*$numberDays;
   // $cost =  0;





    return [

        'name' => $faker->firstName,
        'email' => $faker->companyEmail,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'numOfPeople' => $faker->numberBetween(1,5),
        'paymentType' =>$faker->randomElement(["direct","card","netbanking"]) ,
        'duration' =>  $numberDays,
        'roomType' => $roomType[$room_random],
        'roomNumber' => $faker->numberBetween(0,100),
        'startDate' => $start_date,
        'endDate' => $end_date,
        'price' =>  $roomType[$room_random],
        'services' => $faker->randomElement(["rent","food","others"]),
        'total' =>(string)$cost,


    ];
});



$factory->define(App\Models\Ac_room::class, function (Faker\Generator $faker) {





    return [




    ];
});

$factory->define(App\Models\Ac_customer::class, function (Faker\Generator $faker) {
    return [
    ];
});

