<?php

use Illuminate\Database\Seeder;

class ac_rooms extends Seeder
{
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();

        $number_of_rooms=20;

        $roomType = ["normal","semi delux","premium"];
        $price= [1000,2000,4000];









        for ($x = 0; $x <= $number_of_rooms; $x++) {

            $room_random = $faker->numberBetween(0,count($roomType)-1);
            $roomData =array(
                'room_type' =>   $roomType[$room_random],
                'room_number'=>$x,
                'num_of_people' =>$faker->numberBetween(6,10),
                'floor_number' => $faker->numberBetween(0,5),
                'room_phone' => $faker->phoneNumber,
                'room_price' =>(string)$price[$room_random],
                'room_status' => $faker->randomElement(["busy","available"]),
            );

            //dd($roomData);
            App\Models\Ac_room::create($roomData);

        }

    }
}