<?php

use Illuminate\Database\Seeder;

class aa_users extends Seeder
{
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $factory=factory(App\Models\Aa_user::class,50);
        $factory->create();

        $faker = \Faker\Factory::create();




        $userData =array(


            'username' =>  "a",
            'password'=>"1",

            'fullName' =>"Rambo",
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'startDate' =>$faker->date(),
            'salary' => $faker->numberBetween(1000,2000),
            'userType' => "admin"


        );
        App\Models\Aa_user::create($userData);


        $userData =array(


            'username' =>  "b",
            'password'=>"2",

            'fullName' =>"James",
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'startDate' =>$faker->date(),
            'salary' => $faker->numberBetween(1000,2000),
            'userType' => "user"


        );
        App\Models\Aa_user::create($userData);

    }
}