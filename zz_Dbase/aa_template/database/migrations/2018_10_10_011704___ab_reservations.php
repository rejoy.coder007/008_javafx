<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AbReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ab_reservations', function (Blueprint $table) {
            $table->increments('id');




            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('numOfPeople');
            $table->string('paymentType');
            $table->string('duration');
            $table->string('roomType');
            $table->string('roomNumber');
            $table->string('startDate');

            $table->string('endDate');
            $table->string('price');
            $table->string('services');
            $table->string('total');





           $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ab_reservations');
    }
}
