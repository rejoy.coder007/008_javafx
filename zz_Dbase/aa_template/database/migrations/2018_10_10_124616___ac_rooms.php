<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ac_rooms', function (Blueprint $table) {
            $table->increments('room_id');
            $table->string('room_type');
            $table->string('room_number');
            $table->string('num_of_people');
            $table->string('floor_number');
            $table->string('room_phone');
            $table->string('room_price');
            $table->string('room_status');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ac_rooms');
    }
}
