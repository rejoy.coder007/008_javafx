<header class="header">
   {{-- <img class="logo" src="03_IMAGES/ZB_PART_TWO/logo.png" alt="trillo logo">--}}
@svg('resources/views/zy_SVG/ZB_PART_TWO/aa_header/aa_magnifying-glass',  ['class' => 'logo','id'=>"IMG_LOGO"])

    <form class="search" action="#">
        <input class="search__input" type="text" placeholder="Search hotels">
        <button class="search__button">

            {{--
            <svg class="search__icon">
                <use xlink:href="03_IMAGES/ZB_PART_TWO/sprite.svg#icon-magnifying-glass"></use>
            </svg>
             --}}
            @svg('resources/views/zy_SVG/ZB_PART_TWO/aa_header/aa_magnifying-glass',  ['class' => 'search__icon','id'=>"IMG_MAG"])
        </button>
    </form>

    <nav class="user-nav">
        <div class="user-nav__icon-box">

            {{--
              <svg class="user-nav__icon">

                <use xlink:href="03_IMAGES/ZB_PART_TWO/sprite.svg#icon-bookmark"></use>
            </svg>

             --}}
            @svg('resources/views/zy_SVG/ZB_PART_TWO/aa_header/bb_bookmark',  ['class' => 'user-nav__icon','id'=>"IMG_BKMARK"])

            <span class="user-nav__notification">7</span>
        </div>
        <div class="user-nav__icon-box">

            {{-- <svg class="user-nav__icon">
                <use xlink:href="03_IMAGES/ZB_PART_TWO/sprite.svg#icon-chat"></use>
            </svg>--}}
            @svg('resources/views/zy_SVG/ZB_PART_TWO/aa_header/cc_chat',  ['class' => 'user-nav__icon','id'=>"IMG_CHAT"])

            <span class="user-nav__notification">13</span>
        </div>
        <div class="user-nav__user">
            {{--


            <img class="user-nav__user-photo" src="03_IMAGES/ZB_PART_TWO/user.jpg" alt="User photo">
            --}}
           {{--  @svg('resources/views/zy_SVG/ZB_PART_TWO/aa_header/dd_user',  ['class' => 'user-nav__user-photo','id'=>"IMG_USER"])


            @include('zx_BASE_64.ZB_PART_TWO.aa_header.aa_user_image')--}}
            <img class="user-nav__user-photo" src="03_IMAGES/ZB_PART_TWO/user.jpg" alt="User photo">
            <span class="user-nav__user-name"><a href="/flexbox2"></a>rvm </span>
        </div>
    </nav>

</header>