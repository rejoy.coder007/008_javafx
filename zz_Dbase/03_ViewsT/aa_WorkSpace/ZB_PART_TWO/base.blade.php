<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name=description content="">
    <meta name=author content="">
<!--
    <script defer="defer">alert(2);</script>
    <script>alert(1)</script>
    <script defer="defer">alert(3);</script>
-->
    <script defer type="module" src="02_SCRIPTS/zb_part_two.js"></script>
    <link rel="shortcut icon" type="image/png" href="03_IMAGES/favicon.png">
    <title>@yield('title')</title>
    {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">--}}
    <style>  @include('01_CSS.zb_part_two') </style>



</head>


<body style="background-color: green">

{{--
<!-- Source -->
<input required class="input-email" type="email" id="inputEmail" name="email">

<input required class="input-email" type="email" id="inputEmail" name="email"  >
<script src="02_SCRIPTS/jquery-0.js"></script>
<script src="02_SCRIPTS/jquery-1.js"></script>
--}}

@yield('content')

<button id="TestID" style="background-color: yellow;height: 50px"> YES</button>



{{--
<script>
    @include('00_SCRIPTS.zb_part_two')
</script>

<script src="02_SCRIPTS/ad_class.js"></script>
--}}



</body>

</html>