@extends('AA_TEST_SAMPLE.base')

@section('title')
    Test
@endsection

@section('laravelBlock')
    @include('AA_TEST_SAMPLE.aa_include.zd_laravel_files')
@endsection

@section('cssBlock')

@endsection

@section('content')


    <div class="  container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @include('AA_TEST_SAMPLE.aa_include.zc_navbar')
            </div>

        </div>

    </div>




    <br>
    <br>
    @include('zz_body.AA_TEST_SAMPLE.body')




@endsection


@section('bottomJS')



@endsection


 
  