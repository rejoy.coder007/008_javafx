@extends('AAA_MARKO.base')

@section('title')
    Test
@endsection

@section('laravelBlock')
    @include('AAA_MARKO.aa_include.zd_laravel_files')
@endsection

@section('cssBlock')

@endsection

@section('content')


    <div class="  container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @include('AAA_MARKO.aa_include.zc_navbar')
            </div>

        </div>

    </div>




    <br>
    <br>
    @include('zz_body.AAA_MARKO.body')




@endsection


@section('bottomJS')



@endsection


 
  