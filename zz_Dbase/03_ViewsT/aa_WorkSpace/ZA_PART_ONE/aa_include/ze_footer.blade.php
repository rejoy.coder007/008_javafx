<footer class="footer">
    <div class="footer__logo-box">
        <picture class="footer__logo">
            <source srcset="03_IMAGES/ZA_PART_ONE/logo-green-small-1x.png 1x, 03_IMAGES/ZA_PART_ONE/logo-green-small-2x.png 2x"
                    media="(max-width: 37.5em)">
            <img srcset="03_IMAGES/ZA_PART_ONE/logo-green-1x.png 1x, 03_IMAGES/ZA_PART_ONE/logo-green-2x.png 2x" alt="Full logo" src="img/logo-green-2x.png">
        </picture>
    </div>
    <div class="row">
        <div class="col-1-of-2">
            <div class="footer__navigation">
                <ul class="footer__list">
                    <li class="footer__item"><a href="#" class="footer__link">Company</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Contact us</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Carrers</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Privacy policy</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Terms</a></li>
                </ul>
            </div>
        </div>
        <div class="col-1-of-2">
            <p class="footer__copyright">
                Built by <a href="#" class="footer__link">RVM</a>   <a href="#" class="footer__link"> XX</a>.
                Copyright &copy; by XX.  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Sed sed risus pretium quam. Aliquam sem et tortor consequat id. Volutpat odio facilisis mauris sit
                amet massa vitae. Mi bibendum neque egestas congue. Placerat orci nulla pellentesque dignissim enim
                sit. Vitae semper quis lectus nulla at volutpat diam ut venenatis. Malesuada pellentesque elit eget
                gravida cum sociis natoque penatibus et. Proin fermentum leo vel orci porta non pulvinar neque laoreet.
                Gravida neque convallis a cras semper. Molestie at elementum eu facilisis sed odio morbi quis. Faucibus
                vitae aliquet nec ullamcorper sit amet risus nullam eget. Nam libero justo laoreet sit. Amet massa
                vitae tortor condimentum lacinia quis vel eros donec. Sit amet facilisis magna etiam. Imperdiet sed
                euismod nisi porta.
            </p>
        </div>
    </div>
</footer>
