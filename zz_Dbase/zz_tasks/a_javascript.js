 

             
var SCRIPTS_IN_PATH = '02_SCRIPTS/**/*.*';

var jshint = require('gulp-jshint');

var notify = require('gulp-notify');

var gulp                                                      = require('gulp');
var notify                                             = require('gulp-notify');
var concat                                             = require('gulp-concat');
var sourcemaps                                     = require('gulp-sourcemaps');
var gulpif                                                 = require('gulp-if');
///var uglify                                             = require('gulp-uglify');
let uglify = require('gulp-uglify-es').default;
var prettify                                     = require('gulp-jsbeautifier');
var removeEmptyLines                       = require('gulp-remove-empty-lines');

var LIB_JAVASCRIPT_IN_PATH                                   = '02_SCRIPTS/LIB/**/*.*';
var LIB_JAVASCRIPT_OUT_PATH                   = 'aa_template/public/02_SCRIPTS/LIB';
var LIB_SCRIPTS_OUT_FILE_NAME           = 'zz_common_functional_programming.js';
var plumber = require('gulp-plumber');

const {src, task} = require('gulp');
const eslint = require('gulp-eslint');


module.exports =
{


            javaScriptError: function ()
            {

                var result =
                        gulp.src(SCRIPTS_IN_PATH)




                    // eslint() attaches the lint output to the "eslint" property
                    // of the file object so it can be used by other modules.
                        .pipe(eslint())
                        .pipe(plumber())
                        // eslint.format() outputs the lint results to the console.
                        // Alternatively use eslint.formatEach() (see Docs).
                        .pipe(eslint.format())
                        // To have the process exit with an error code (1) on
                        // lint error, return the stream and pipe to failAfterError last.
                       // .pipe(eslint.failAfterError())
                        .on('error', notify.onError({ message: 'There is a JS error, please look the console for details'}));


                console.log('JS Error Ended Started -----------------------------------------------------');

                return result;

            },
            javaScriptMoveLib: function ()
            {

                var result =
                    gulp.src(  LIB_JAVASCRIPT_IN_PATH)
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.init()))

                        .pipe(gulpif(process.env.NODE_ENV === 'production', uglify()))

                        /*
                                                .pipe(gulpif(process.env.NODE_ENV === 'development',removeEmptyLines()))

                                                .pipe(gulpif(process.env.NODE_ENV === 'development',
                                                    prettify({
                                                        mode: 'VERIFY_AND_WRITE'
                                                    }),
                                                    uglify()

                                                ))
                        */
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write()))
                        .pipe(gulp.dest(LIB_JAVASCRIPT_OUT_PATH ));
                //      .pipe(notify({message: 'home page Javascript task complete'}));

                console.log("CCCCCCCCCCCCCC");
                return result;


            }
            
            
            
 


 };

 
global.javaScript = function (options)
{
    
                    var result =
                        gulp.src(  options.SCRIPTS_IN_PATH)
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.init()))
                        .pipe(concat(options.SCRIPTS_OUT_FILE_NAME))
                        .pipe(gulpif(process.env.NODE_ENV === 'production', uglify()))


/*
                        .pipe(removeEmptyLines())


                        .pipe(gulpif(process.env.NODE_ENV === 'development',
                                prettify({
                                    mode: 'VERIFY_AND_WRITE'
                                }),
                                uglify()

                                ))
*/
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write()))
                        .pipe(gulp.dest(options.SCRIPTS_OUT_PATH ));
                  //      .pipe(notify({message: 'home page Javascript task complete'}));

    console.log("CCCCCCCCCCCCCC");
                return result;


    
    //console.log("Test");
};

/*
javaScriptError: function ()
{

    var result =
        gulp.src(SCRIPTS_IN_PATH)
            .pipe(jshint({

                "esversion": 6,
                expr: true


            }))
            // Use gulp-notify as jshint reporter
            .pipe(notify(function (file)
            {
                if (file.jshint.success)
                {
                    // Don't show something if success
                    return false;
                }

                var errors = file.jshint.results.map(function (data)
                {
                    if (data.error)
                    {
                        return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
                    }
                }).join("\n");

                return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
            }));
    console.log('JS Error Ended Started -----------------------------------------------------');

    return result;

}
*/

     