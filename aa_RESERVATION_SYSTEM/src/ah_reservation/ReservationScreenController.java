package ah_reservation;





import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import com.mysql.jdbc.PreparedStatement;

import aa_config.Config_project;
import ad_admin_screen.AdminScreenController;
import ai_room.RoomScreenController;
import az_connection_db.DBConnection;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;


public class ReservationScreenController implements Initializable {

	@FXML
	private JFXTextField name;
	@FXML
	private JFXTextField phone;
	@FXML
	private JFXTextField address;
	@FXML
	private JFXTextField email;
	@FXML
	private JFXTextField duration;
	@FXML
	private JFXTextField num_people;
	@FXML
	private JFXTextField paymentType;
	@FXML
	private JFXTextField roomType;
	@FXML
	private JFXTextField roomNumber;
	@FXML
	private JFXTextField price;
	@FXML
	private JFXTextField services;
	@FXML
	private JFXTextField total;
	@FXML
	private JFXDatePicker startDate;
	@FXML
	private JFXDatePicker endDate;
	@FXML
	private StackPane stackepane;

	/**
	 * Initializes the controller class.
	 */
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {
		 // TODO
		 name.setText("");
		 email.setText("");
		 address.setText("");
		 phone.setText("");
		 paymentType.setText("");
		 num_people.setText("");
		 roomNumber.setText("");
		 roomType.setText("");
		 price.setText("");
		 total.setText("");
		 startDate.setValue(LocalDate.now());
		 endDate.setValue(LocalDate.now().plusMonths(1));
		 services.setText("");
		 duration.setText("");
	 }    

	 @FXML
	 private void back(MouseEvent event) {

		 
        Stage home=new Stage();
        Parent root=null;

        try {
            root=FXMLLoader.load(getClass().getResource("../ae_home_screen/HomeScreen.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Stage current=(Stage)roomNumber.getScene().getWindow();
        Scene scene=new Scene(root);

        home.setScene(scene);
    	if(!Config_project.TEST)
        home.initStyle(StageStyle.TRANSPARENT);

        current.hide();
        home.show();
	 }

	 @FXML
	 private void close(MouseEvent event) {

		 
        JFXDialogLayout dialogLayout=new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Close"));
        dialogLayout.setBody(new Text("Do You want to exit !"));

        JFXButton ok=new JFXButton("Ok");
        JFXButton Cancel=new JFXButton("Cancel");

        JFXDialog dialog=new JFXDialog(stackepane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });

        Cancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });

        dialogLayout.setActions(ok,Cancel);
        dialog.show(); 
	 }

	 @FXML
	 private void rest(MouseEvent event) {

		 name.setText("");
		 email.setText("");
		 address.setText("");
		 phone.setText("");
		 paymentType.setText("");
		 num_people.setText("");
		 roomNumber.setText("");
		 roomType.setText("");
		 price.setText("");
		 total.setText("");
		 startDate.setValue(LocalDate.now());
		 endDate.setValue(LocalDate.now());
		 services.setText("");
		 duration.setText("");


	 }

	 @FXML
	 private void book(MouseEvent event) {

		 
		
		 int res=0;
		 String sql="INSERT INTO ab_reservations (name,email,address,phone,numOfPeople,paymentType,duration,roomType,roomNumber,startDate,endDate,price,services,total,created_at,updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 Connection connection=DBConnection.getConnection();
		// System.out.println(endDate.getValue().toString());
		 System.out.println(endDate.getValue().toString());
		 try {
			
			 PreparedStatement ps=(PreparedStatement)connection.prepareStatement(sql);
			 
			 System.out.println(name.getText());
	 	 
			 ps.setString(1, name.getText().toString());
			 
			 ps.setString(2, email.getText().toString());
			 ps.setString(3, address.getText().toString());
			 ps.setString(4, phone.getText().toString());
			 ps.setString(5, num_people.getText().toString());
			 
			 ps.setString(6, paymentType.getText().toString());
			 ps.setString(7, duration.getText().toString());
			 ps.setString(8, roomType.getText().toString());
			 ps.setString(9, roomNumber.getText().toString());
			 ps.setString(10, startDate.getValue().toString());
			 ps.setString(11, startDate.getValue().toString());
		
			 
		 
		    ps.setString(12, price.getText().toString());
			
			 ps.setString(13, services.getText().toString());
			 ps.setString(14, total.getText().toString());
			 
			 ps.setString(15, LocalDate.now().toString());
			 ps.setString(16, LocalDate.now().toString());

			 
			 res=ps.executeUpdate();
			 
			 /*
			 System.out.println(name.getText());
			 System.out.println(email.getText());
			 System.out.println(address.getText());
			 System.out.println(phone.getText());
			 System.out.println(paymentType.getText());
			 System.out.println(num_people.getText());
			 System.out.println(roomNumber.getText());
			 System.out.println(roomType.getText());
			 System.out.println(price.getText());
			 System.out.println(total.getText());
			
			 System.out.println(startDate.getValue().toString());
			 System.out.println(endDate.getValue().toString());
			 System.out.println(services.getText());
			 System.out.println( duration.getText());*/

		

		 } catch (SQLException ex) {
			 Logger.getLogger(ReservationScreenController.class.getName()).log(Level.SEVERE, null, ex);
		 }
 
		 if(res>0){
			 Image image=new Image("img/mooo.png");
			 Notifications notification=Notifications.create()
					 .title("Done")
					 .text("Data Added Scussfully")
					 .hideAfter(Duration.seconds(3))
					 .position(Pos.BOTTOM_LEFT)
					 .graphic(new ImageView(image));
			 notification.darkStyle();
			 notification.show();
			 updateStatus();
		 }else{
			 Image image=new Image("img/delete.png");
			 Notifications notification=Notifications.create()
					 .title("Error")
					 .text("Something wrong")
					 .hideAfter(Duration.seconds(3))
					 .position(Pos.BOTTOM_LEFT)
					 .graphic(new ImageView(image));
			 notification.darkStyle();
			 notification.show();
		 }

 


	 }

	 

	 private void updateStatus() {
		 
		 
		 String text=roomNumber.getText().toString().trim();
		 String sql="UPDATE ac_rooms SET room_number=? WHERE room_number=?";
		 Connection connection=DBConnection.getConnection();
		 try {
			 PreparedStatement ps=(PreparedStatement)connection.prepareStatement(sql);
			 ps.setString(1, "busy");
			 ps.setString(2, text);

			 ps.executeUpdate();


		 } catch (SQLException ex) {
			 Logger.getLogger(RoomScreenController.class.getName()).log(Level.SEVERE, null, ex);
		 }
 
	 }

	 
 

 

}
