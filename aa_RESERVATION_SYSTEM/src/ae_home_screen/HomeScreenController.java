package ae_home_screen;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;

import aa_config.Config_project;
import ad_admin_screen.AdminScreenController;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class HomeScreenController implements Initializable {

	@FXML
	private Pane pane_1;
	@FXML
	private Pane pane_2;
	@FXML
	private Pane pane_3;
	@FXML
	private Pane pane_4;
	@FXML
	private Pane pane_5;
	@FXML
	private StackPane stackepane;



	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}    

	@FXML
	private void mouse_exit_1(MouseEvent event) {
		pane_1.setStyle("-fx-background-color: white; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_hover_1(MouseEvent event) {
		pane_1.setStyle("-fx-background-color: #377ce8; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_exit_2(MouseEvent event) {
		pane_2.setStyle("-fx-background-color: white; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_hover_2(MouseEvent event) {
		pane_2.setStyle("-fx-background-color: #377ce8; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_exit_3(MouseEvent event) {
		pane_3.setStyle("-fx-background-color: white; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_hover_3(MouseEvent event) {
		pane_3.setStyle("-fx-background-color: #377ce8; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_exit_4(MouseEvent event) {
		pane_4.setStyle("-fx-background-color: white; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_hover_4(MouseEvent event) {
		pane_4.setStyle("-fx-background-color: #377ce8; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_exit_5(MouseEvent event) {
		pane_5.setStyle("-fx-background-color: white; -fx-background-radius: 6px;");
	}

	@FXML
	private void mouse_hover_5(MouseEvent event) {
		pane_5.setStyle("-fx-background-color: #377ce8; -fx-background-radius: 6px;");
	}

	@FXML
	private void ReservationScreen(MouseEvent event) {


		Stage reservation=new Stage();
		Parent root=null;

		try {
			root=FXMLLoader.load(getClass().getResource("../ah_reservation/ReservationScreen.fxml"));
		} catch (IOException ex) {
			Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
		}

		Stage current=(Stage)pane_1.getScene().getWindow();
		Scene scene=new Scene(root);

		reservation.setScene(scene);
		if(!Config_project.TEST)
			reservation.initStyle(StageStyle.TRANSPARENT);

		current.hide();
		reservation.show(); 
	}

	@FXML
	private void RoomScreen(MouseEvent event) {


		Stage room=new Stage();
		Parent root=null;

		try {
			root=FXMLLoader.load(getClass().getResource("../ai_room/RoomScreen.fxml"));
		} catch (IOException ex) {
			Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
		}

		Stage current=(Stage)pane_1.getScene().getWindow();
		Scene scene=new Scene(root);

		room.setScene(scene);

		if(!Config_project.TEST)
			room.initStyle(StageStyle.TRANSPARENT);

		current.hide();
		room.show(); 
	}

	@FXML
	private void customerScreen(MouseEvent event) {


		Stage customer=new Stage();
		Parent root=null;

		try {
			root=FXMLLoader.load(getClass().getResource("../ak_customer/CustomersScreen.fxml"));
		} catch (IOException ex) {
			Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
		}

		Stage current=(Stage)pane_1.getScene().getWindow();
		Scene scene=new Scene(root);

		customer.setScene(scene);
		 if(!Config_project.TEST)
		customer.initStyle(StageStyle.TRANSPARENT);

		current.hide();
		customer.show(); 
	}

	@FXML
	private void logout(MouseEvent event) {

		JFXDialogLayout dialogLayout=new JFXDialogLayout();
		dialogLayout.setHeading(new Text("Alert"));
		dialogLayout.setBody(new Text("Do You want to Logout !"));

		JFXButton ok=new JFXButton("Ok");
		JFXButton Cancel=new JFXButton("Cancel");

		JFXDialog dialog=new JFXDialog(stackepane, dialogLayout, JFXDialog.DialogTransition.CENTER);

		ok.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {


				Stage home=new Stage();
				Parent root=null;

				try {
					root=FXMLLoader.load(getClass().getResource("../ac_login/Login.fxml"));
				} catch (IOException ex) {
					Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
				}

				Stage current=(Stage)pane_1.getScene().getWindow();
				Scene scene=new Scene(root);

				home.setScene(scene);
				home.initStyle(StageStyle.TRANSPARENT);

				current.hide();
				home.show();
			}
		});

		Cancel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				dialog.close();
			}
		});

		dialogLayout.setActions(ok,Cancel);
		dialog.show();
	}

	@FXML
	private void exit(MouseEvent event) {
		JFXDialogLayout dialogLayout=new JFXDialogLayout();
		dialogLayout.setHeading(new Text("Close"));
		dialogLayout.setBody(new Text("Do You want to exit !"));

		JFXButton ok=new JFXButton("Ok");
		JFXButton Cancel=new JFXButton("Cancel");

		JFXDialog dialog=new JFXDialog(stackepane, dialogLayout, JFXDialog.DialogTransition.CENTER);

		ok.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});

		Cancel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				dialog.close();
			}
		});

		dialogLayout.setActions(ok,Cancel);
		dialog.show();
	}



}
